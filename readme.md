Tech Stack:
BE:

- Postgres
- Sequelize
- JWT
- Bcrypt
- Midtrans / Xendit (optional)

FE:

- Scss
- Boostrap
- Ejs
- Dom

Design Pattern = MVCR
Deploy = Heroku
Repository Gitlab = https://gitlab.com/ajisetiaji101/ardawalika
